

from random import random


class ListRandom:
    def __init__(self, list):
        self.list = list

    def insert(self, value):
        if value not in self.list:
            self.list.append(value)

    def remove(self, value):
        self.list.remove(value)

    def GetRandom(self):
        return random.choice(self.list)