from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium import webdriver
from oauth2client.service_account import ServiceAccountCredentials
from flask import Flask, request
import requests as req
import gspread
import gitlab
from time import sleep
from threading import Thread
from string import digits
import urllib.request
import platform
import pickle
import os
import logging
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException
from flask_cors import CORS
from flask import jsonify
from os import path
import uuid
import sys
import subprocess
import shutil
import random
import datetime
try:
    import json
except:
    pass
try:
    import pgeocode
except:
    pass
try:
    import webbrowser
except:
    pass
try:
    from webdriver_manager.chrome import ChromeDriverManager
except Exception:
    pass
try:
    from webdriver_manager.core.utils import ChromeType
except Exception:
    pass


VERSIONPROG = "1.0.2"
i3 = False
quota = "300"
VERLIST = [
    "131.0.6778.69",
    "129.0.6668.89",
    "129.0.6668.70",
    "116.0.5845.96",
    "128.0.6613.86",
    "126.0.6478.61",
    "127.0.6533.4",
    "125.0.6422.61",
    "125.0.6422.60",
    "124.0.6367.60",
    "123.0.6312.122",
    "122.0.6261.57",
    "121.0.6167.184",
    "121.0.6167.85",
    "120.0.6099.71",
]

try: 
    r = req.get("https://googlechromelabs.github.io/chrome-for-testing/last-known-good-versions-with-downloads.json")
    r = r.json()
    stable_version = r["channels"]["Stable"]["version"]
    VERLIST.insert(0, str(stable_version))
except:
    pass
try: 
    r = req.get("https://googlechromelabs.github.io/chrome-for-testing/last-known-good-versions-with-downloads.json")
    r = r.json()
    stable_version = r["channels"]["Stable"]["version"]
    VERLIST.insert(0, str(stable_version))
except:
    pass
idAnnouncing = ""
app_creds_dictionary = {
    "type": "service_account",
    "project_id": "facilitimo",
    "private_key_id": "04eeb172c40d2cefbc91c1ff8df8af7ee564bc94",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC5FOeMzVxiEZ3/\nHTnrBvkzBh/ShLfESTeFEIj86PVcieyVBaU9lROF+Nd5eYyjaoKTMDLoe16f5PmE\nBJdleHL6DLiJF0wCo//g5rVoMVNDIQm1TwtIzEkrI/MjbOANB9FG/H/8gmnyEPbP\nkyny+p2yzaPs3YYmcbHRPt6X/MC6XjYbzG0FVg82+i1vES/WJ2NO4gjeLBniAHme\n6efRJrWx2bsQ1ls+QDWL1O1LD0IW/XZ+ypbHDOia4tghdb3Lbx3yQrRDq0J8HKuV\nZu+MjiiZvyIEF5+v3M3T8ufBZOFQRtt9Fxu7gADwVBcbezc+wSK2UCjkMlYr40Ac\nAgL0+DsHAgMBAAECggEADcaejX5jl/ETv39fZjz4o3GbQMIcI4IHneHjMF/uk1q4\nv0bSS8ki0N/6udHwQ+GTonz97QvxuRkD8W2eDzFt7Wqw+ZNAVxn527WQXxb8yYxV\n+qbNgjeiYpGkcUiTTKT+ULhOlm7mj9vvYvfjMXCx+DMSNnkumQHxaE+zpcBsX/Ku\naN/6+Nlp/c0JhjF0TfESow0snAx0Ctb8F9qr9ajNNZP22BjoNhyuKRIpfU5oaMNI\n2JXZblTPQqlHXO6MPirr+Sf/EHd7MgfTGKivw+vOXMCXRPlW4/nuBOzHVsYiDgMH\nuvPLtT0QufuWaHA2KNhYADWR3IiFfN1pnWBQS56isQKBgQD/AN1Yjsudgk9rCgbJ\ne3K5UnQ6BCJ5+9vM26QPsG23H9sbHAzFM/myQ71yyd1cOtTUwUuRK87XSV+lyPfk\nuNDdHjQ8vc1EVLMBsA/V7nhtCHGxsTjEaNxSLIZW5y1gO5qRL6Gmuyu07brti3ok\nAqyFuJC7tecuwQP0C/esO9mQRQKBgQC5zhT6mV4bQk96iWknTe7jFA4Z8Ixmu3mk\n856mrH2lVV7aGfTrNwwJA9mgh0m9LbrCEzZ+MnqL8NCoGdWjgUp8puzDR8557bxQ\nVrWl0EddSIOz1iUVJZTLEI2dXcK+ebGNRMWc1Qt0rhaR/1XaL3VyqbJNeikKYB51\nYDwAqJeQ2wKBgQDJL+tyxSaMXJVABCcaq5T19cHRTBXPXBAsblXgMvg89Jsvpk/E\n/p7QMHW1rOJCs//a4641GPUWEcOuzwIVqGLRMdVQ3wamFJUujbPQu1az32ekJAh/\nnaPGCW7r/XRlamEmjwfgvedb1fwXEmYJJzrqPZWjI38D+jCeSRrc/lNGaQKBgBVH\nxxtUiXSxyYyNRF/eCygy2dKavFnfyvTbeiY4x0MyXeS46FYwPc6ihEBgfk+Odz9X\nF/yoJ2TztdIq808AtdlE/QT4qmtIkTLaW3+cNTA35+2m7yRW443GXZHSdUV/hphZ\nchCeTeuVLLyX8+tDWD5DtUouXZj0nUhwNy8UIBfVAoGAe+LQkND3TmDvmCHF2Nkw\nW5Kloj5R+XS1SRcEsOwVw+Mn/WQLJj9bcNgpqTgpIx/UXcGf6Fzn3Lj+ILP+Yp/j\nQS7iyDheLowxebdjBjIOGjgE6cpVr1/XpQicBx+kwJU/LzeCtybshrBEB0j7RIKY\nfpZ0RHeBB+poDjuKuvITVkQ=\n-----END PRIVATE KEY-----\n",
    "client_email": "facilitimo@facilitimo.iam.gserviceaccount.com",
    "client_id": "112556907095946162829",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/facilitimo%40facilitimo.iam.gserviceaccount.com",
}
# creds = ServiceAccountCredentials.from_json_keyfile_dict(app_creds_dictionary, scope)
driverF1 = None


def uploadFile(project_id, file_path, fileName):
    try:
        gl = gitlab.Gitlab(
            "https://gitlab.com", private_token="glpat-hiMn8ygdesvq_4a3tuNr"
        )
        gl.auth()
        with open(file_path, "r") as my_file:
            file_content = my_file.read()
        project = gl.projects.get(project_id)
        try:
            a = project.files.get(file_path=fileName, ref="main")
            f = project.files.update(
                file_path=fileName,
                new_data={
                    "file_path": fileName,
                    "branch": "main",
                    "content": file_content,
                    "author_email": "danhabib011@gmail.com",
                    "author_name": "dd",
                    "commit_message": "Upload",
                },
            )
        except Exception as e:
            f = project.files.create(
                {
                    "file_path": fileName,
                    "branch": "main",
                    "content": file_content,
                    "author_email": "danhabib011@gmail.com",
                    "author_name": "dd",
                    "commit_message": "Upload",
                }
            )

        return True
    except Exception as e:
        return False
    pass


class AppImoAuto:
    def __init__(self, userName, passW, fileName, fileNameName, txt, excelLink):
        options = Options()
        # options.add_argument("--headless")
        self.driver = driverF1
        self.fileName = fileName
        self.fileNameName = fileNameName
        self.actions = None
        self.userName = userName
        self.passW = passW
        self.txt = txt
        self.excelLink = excelLink

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    # def init(self):
    #     options = Options()
    #     options.add_experimental_option("excludeSwitches", ["enable-logging"])
    #     options.add_argument("user-data-dir=" + os.getcwd() + "/apimo")
    #     # self.driver = webdriver.Chrome(ChromeDriverManager().install(),
    #     #                                options=options)
    #     c = 0
    #     if str(uuid.getnode()) == "180725258811321":
    #         try:  # remove chromedriver_mac_arm64.zip folder
    #             shutil.rmtree(os.getcwd() + "/chromedriver_mac64.zip")
    #         except:
    #             pass
    #         driverFile = urllib.request.urlretrieve(
    #             "https://chromedriver.storage.googleapis.com/103.0.5060.134/chromedriver_mac64.zip",
    #             os.getcwd() + "/chromedriver_mac64.zip",
    #         )
    #         shutil.unpack_archive(os.getcwd() + "/chromedriver_mac64.zip", os.getcwd())
    #         os.chmod(os.getcwd() + "/chromedriver", 754)
    #         c = 2
    #     else:
    #         try:
    #             try:
    #                 self.driver = webdriver.Chrome(
    #                     ChromeDriverManager().install(), options=options
    #                 )
    #                 c = 1
    #             except:
    #                 self.driver = webdriver.Chrome(
    #                     ChromeDriverManager().install(), options=options
    #                 )
    #         except Exception as e:
    #             logging.info("error")
    #     if c == 0:
    #         for ver in VERLIST:
    #             try:
    #                 if platform.system() == "Windows":
    #                     driverFile = urllib.request.urlretrieve(
    #                         f"https://storage.googleapis.com/chrome-for-testing-public/{ver}/win32/chromedriver-win32.zip",
    #                         os.getcwd() + "/chromedriver_win3222.zip",
    #                     )
    #                     shutil.unpack_archive(
    #                         os.getcwd() + "/chromedriver_win3222.zip", os.getcwd()
    #                     )
    #                     os.chmod(
    #                         os.getcwd() + "/chromedriver-win32/chromedriver.exe", 754
    #                     )
    #                     os.rename(
    #                         os.getcwd() + "/chromedriver-win32/chromedriver.exe",
    #                         "chromedriver.exe",
    #                     )
    #                     self.driver = webdriver.Chrome(
    #                         os.getcwd() + "/chromedriver.exe", options=options
    #                     )
    #                 else:
    #                     if platform.processor() == "arm64":
    #                         try:
    #                             # remove chromedriver_mac_arm64.zip folder
    #                             shutil.rmtree(os.getcwd() + "/chromedriver-mac-arm64")
    #                         except:
    #                             pass
    #                         driverFile = urllib.request.urlretrieve(
    #                             f"https://storage.googleapis.com/chrome-for-testing-public/{ver}/mac-arm64/chromedriver-mac-arm64.zip",
    #                             os.getcwd() + "/chromedriver_mac_arm64.zip",
    #                         )
    #                         shutil.unpack_archive(
    #                             os.getcwd() + "/chromedriver_mac_arm64.zip", os.getcwd()
    #                         )

    #                         os.chmod(
    #                             os.getcwd() + "/chromedriver-mac-arm64/chromedriver",
    #                             754,
    #                         )
    #                         os.rename(
    #                             os.getcwd() + "/chromedriver-mac-arm64/chromedriver",
    #                             "chromedriver",
    #                         )
    #                     elif "arm" in platform.processor():
    #                         try:
    #                             # remove chromedriver_mac_arm64.zip folder
    #                             shutil.rmtree(os.getcwd() + "/chromedriver-mac-arm64")
    #                         except:
    #                             pass
    #                         driverFile = urllib.request.urlretrieve(
    #                             f"https://storage.googleapis.com/chrome-for-testing-public/{ver}/mac-arm64/chromedriver-mac-arm64.zip",
    #                             os.getcwd() + "/chromedriver_mac_arm64.zip",
    #                         )
    #                         shutil.unpack_archive(
    #                             os.getcwd() + "/chromedriver_mac_arm64.zip", os.getcwd()
    #                         )

    #                         os.chmod(
    #                             os.getcwd() + "/chromedriver-mac-arm64/chromedriver",
    #                             754,
    #                         )
    #                         os.rename(
    #                             os.getcwd() + "/chromedriver-mac-arm64/chromedriver",
    #                             "chromedriver",
    #                         )
    #                     else:
    #                         try:
    #                             # remove chromedriver_mac_arm64.zip folder
    #                             shutil.rmtree(os.getcwd() + "/chromedriver_mac64.zip")
    #                         except:
    #                             pass
    #                         driverFile = urllib.request.urlretrieve(
    #                             f"https://storage.googleapis.com/chrome-for-testing-public/{ver}/mac-x64/chromedriver-mac-x64.zip",
    #                             os.getcwd() + "/chromedriver_mac64.zip",
    #                         )
    #                         shutil.unpack_archive(
    #                             os.getcwd() + "/chromedriver_mac64.zip", os.getcwd()
    #                         )
    #                         os.chmod(
    #                             os.getcwd() + "/chromedriver-mac-x64/chromedriver", 754
    #                         )
    #                         os.rename(
    #                             os.getcwd() + "/chromedriver-mac-X64/chromedriver",
    #                             "chromedriver",
    #                         )
    #                     self.driver = webdriver.Chrome(
    #                         os.getcwd() + "/chromedriver", options=options
    #                     )
    #                 break
    #             except Exception as e:
    #                 logging.error(e)
    #                 uploadFile(
    #                     project_id="38114472",
    #                     file_path=self.fileName,
    #                     fileName=self.fileNameName,
    #                 )

    #                 try:
    #                     if platform.system() == "Darwin":

    #                         try:
    #                             os.remove(os.getcwd() + "/chromedriver")
    #                         except:
    #                             pass
    #                     else:
    #                         os.remove(os.getcwd() + "/chromedriver.exe")
    #                 except:
    #                     pass
    #     elif c == 2:
    #         self.driver = webdriver.Chrome(
    #             os.getcwd() + "/chromedriver", options=options
    #         )

    #     try:
    #         self.actions = ActionChains(self.driver)
    #     except Exception:
    #         pass

    def close(self):
        self.driver.close()

    def return_url(self):
        return self.driver.current_url

    def backHome(self):
        self.driver.get("https://espace-client.ubiflow.net/posts/")

    def openEx(self):
        # if platform.system() == "Windows":
        #         self.driver.execute_script(f'window.open("{self.excelLink}", "_blank","toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=0,width=0,height=0");')
        # else:
        webbrowser.open(self.excelLink)
        # webbrowser.open(self.excelLink)

        # self.driver.get(self.excelLink)

    def login(self):
        global driverF1
        self.driver.get("https://espace-client.ubiflow.net/login")
        sleep(1)
        if self.driver.current_url != "https://espace-client.ubiflow.net/":
            while self.driver.current_url != "https://espace-client.ubiflow.net/":
                try:
                    try:
                        self.driver.find_element_by_xpath(
                            "/html/body/div/section/main/form/div[1]/div/div/div/input"
                        ).clear()
                        self.driver.find_element_by_xpath(
                            "/html/body/div/section/main/form/div[2]/div/div/div/input"
                        ).clear()
                    except:
                        pass
                    # username
                    self.driver.find_element_by_xpath(
                        "/html/body/div/section/main/form/div[1]/div/div/div/input"
                    ).send_keys(self.userName)
                    # password
                    self.driver.find_element_by_xpath(
                        "/html/body/div/section/main/form/div[2]/div/div/div/input"
                    ).send_keys(self.passW)
                    # submit
                    self.driver.find_element_by_xpath(
                        "/html/body/div/section/main/form/button"
                    ).click()
                    sleep(1)
                except:
                    sleep(2)
                    pass
        script = """
        for (event_name of ["visibilitychange", "webkitvisibilitychange", "blur"]) {
        window.addEventListener(event_name, function(event) {
                event.stopImmediatePropagation();
            }, true);
        }
        """
        try:
            self.driver.find_element_by_xpath("/html/body/section/section/div/main/span/section/main/div/div/div[1]/div[11]/div/div[1]/button/i").click()
        except:
            pass
        self.driver.execute_script(script)
        return "Connected"

    def addAnnouncing(
        self,
        complementAd,
        refAnnonce,
        ville,
        zipCode,
        adresseDePublication,
        residence,
        type,
        typologie,
        etage,
        metre,
        prix,
        parkingCave,
        surfacePieceAVivre,
        exterieurPrincipal,
        titre,
        tva,
        surfaceExterieurPrincipal,
        chambre,
        phoneNumber,
        nameAgent,
        date,
        surfaceChambrePrincpal,
        nombreSdb,
        situationGeographique,
        pathfirebase,
        idLine,
    ):
        global idAnnouncing, quota
        logging.info("Quota: " + quota)
        # go on page
        # element = WebDriverWait(self.driver, 60).until(
        #     EC.presence_of_element_located(
        #         (By.XPATH, "/html/body/div[2]/header/div[1]/div[2]/div[2]")))
        # sleep(1)
        self.driver.get("https://espace-client.ubiflow.net/posts")
        sleep(2)
        try:
            self.driver.find_element_by_xpath("/html/body/section/section/div/main/span/section/main/div/div/div[1]/div[11]/div/div[1]/button/i").click()
        except:
            pass
        n = 0
        for _ in range(3):
            try:
                a = self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/span/section/main/div/div/form/div[2]/div/div[1]/div[1]"
                )

                n = int(a.text.split("sur")[1].split("\n")[0].strip())
                break
            except:
                sleep(2)
        postQuota = True
        if n >= int(quota):
            logging.info(
                "Vous avez atteint le nombre maximum d'annonces, veuillez en supprimer pour en ajouter de nouvelles."
            )
            uploadFile(
                project_id="38114472",
                file_path=self.fileName,
                fileName=self.fileNameName,
            )
            postQuota = False
            idAnnouncing = "Limite d'annonce atteinte"

        if postQuota:
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/span/section/header/div/section/div[2]/div/a[2]/button"
            ).click()
            sleep(3)
            # INFORMATIONS PRINCIPALES
            # VENTE OU LOCATION
            logging.info("PAGE 1 START")
            try:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[1]/div/div/div/div[1]/input"
                ).click()
            except:
                sleep(3)
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[1]/div/div/div/div[1]/input"
                ).click()
            if residence in ["Location"]:
                self.driver.execute_script(
                    'document.getElementsByTagName("ul")[19].getElementsByTagName("li")[1].click()'
                )
            else:
                self.driver.execute_script(
                    'document.getElementsByTagName("ul")[19].getElementsByTagName("li")[0].click()'
                )
            # REFERENCE
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[1]/div/div/div/div[2]/input"
            ).send_keys(refAnnonce)
            # TITRE
            if titre == "" or titre is None:
                titre = "A VENDRE"
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[3]/div/div/div[1]/div/input"
            ).send_keys(titre)
            # TYPE
            if "studio".upper() in typologie.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Studio")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "appartement".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Appartement")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "maison".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Maison")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "duplex".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Duplex")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "villa".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Villa")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "Local commercial".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Local commercial")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "Bureau".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Bureau")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            elif "Boutique".upper() in type.upper():
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[2]/div/div/div/div/input"
                ).send_keys("Boutique")
                sleep(1)
                self.driver.find_element_by_xpath(
                    "/html/body/div/div[2]/div[1]/ul/li[1]"
                ).click()
            # PRIX
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[4]/div/div/div/div/input"
            ).send_keys(prix.strip())

            indice = 6
            try:
                self.driver.find_element_by_xpath(
                    f"/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[{str(indice)}]/div/div/div/label[1]/span[1]/span"
                ).click()
            except:
                indice += 1
                self.driver.find_element_by_xpath(
                    f"/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[{str(indice)}]/div/div/div/label[1]/span[1]/span"
                ).click()
                pass
            indice += 1
            # SURFACE
            self.driver.find_element_by_xpath(
                f"/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[{str(indice)}]/div/div/div/div[1]/div/input"
            ).send_keys(metre)

            # si location 
            if residence in ["Location"]:
                try:
                    self.driver.find_element_by_xpath("/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[6]/div/div/div/div/label[1]/span[1]/span").click()
                except:
                    pass

            # TEXTE ANNONCE
            logging.info("COMMENTAIRES")

            if tva == "Ok":
                if idLine in ["wuid6fe1-93ca-41a9-8a81-2028d5c3b1ff"]:
                    tva = "ELIGIBLE A LA TVA REDUITE ET PTZ"
                else:
                    tva = "Prix sous conditions d'eligibité"
            else:
                tva = ""

            listData = [
                "Ref_Annonce",
                "Ville",
                "Code_Postal",
                "Adresse de publication",
                "Résidence",
                "Type",
                "Typologie",
                "Etage",
                "Metre",
                "Prix",
                "Tva_reduite",
                "Parking/Cave",
                "Surface_Pièce_à_vivre",
                "Exterieur_principale",
                "Surface_éxterieur",
                "Chambre",
                "Surfaces_Chambre_Principal",
                "Nombe_SDB/WC",
                "Situation_géographique",
                "Titre_annonce",
                "Date_de_livraison",
            ]
            if "PAS DE CHAMBRE" in chambre or chambre == "":
                chambreF = ""
                surfaceChambrePrincpalF = ""
            else:
                chambreF = chambre
                surfaceChambrePrincpalF = str(surfaceChambrePrincpal) + "m²"
            if "NON" in exterieurPrincipal or exterieurPrincipal == "":
                exterieurPrincipalF = ""
                surfaceExterieurPrincipalF = ""
            else:
                exterieurPrincipalF = "Profitez également d’" + exterieurPrincipal
                if surfaceExterieurPrincipal == "":
                    surfaceExterieurPrincipalF = ""
                else:
                    surfaceExterieurPrincipalF = (
                        " de " + surfaceExterieurPrincipal + "m²"
                    )
            desc = (
                self.txt.replace("{", "")
                .replace("}", "")
                .replace("Ref_Annonce", refAnnonce)
                .replace("Ville", ville)
                .replace("Code_Postal", zipCode)
                .replace("Adresse_de_publication", adresseDePublication)
                .replace("Résidence", residence)
                .replace("Type", type)
                .replace("Typologie", typologie)
                .replace("Etage", etage)
                .replace("Metre", metre)
                .replace("Prix", prix)
                .replace("Tva_reduite", tva)
                .replace("Parking/Cave", parkingCave)
                .replace("Surface_Pièce_à_vivre", surfacePieceAVivre)
                .replace("Exterieur_principale", exterieurPrincipalF)
                .replace("Surface_éxterieur", surfaceExterieurPrincipalF)
                .replace("Chambre", chambreF)
                .replace("Surfaces_chambre_Principal", surfaceChambrePrincpalF)
                .replace("Nombe_SDB/WC", nombreSdb)
                .replace("Situation_géographique", situationGeographique)
                .replace("Titre_annonce", titre)
                .replace("date", date)
                .replace("Date_de_livraison", date)
            )
            if idLine in ["aoid6fe1-93ca-41a9-8a81-2028d5c3b1ff"]:
                desc = desc.replace(parkingCave, parkingCave + " dans le prix")
            try:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[10]/div/div/div[1]/div/textarea"
                ).send_keys(str(desc))
            except:
                try:

                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[12]/div/div/div[1]/div/textarea"
                    ).send_keys(str(desc))
                except:
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[1]/div/div/section/main/div[3]/div[11]/div/div/div/div/textarea"
                    ).send_keys(str(desc))

            # CODE POSTAL
            if zipCode == "" or zipCode is None:
                try:
                    zipCode = str(
                        pgeocode.Nominatim("fr").query_location(ville).postal_code
                    ).split(" ")[0]
                except:
                    pass
            try:
                try:
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[1]/div/div/div/div/div/input"
                    ).send_keys(zipCode)
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div"
                    ).click()
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[2]/div/div/div/div/input"
                    ).send_keys(ville)
                except Exception as e:
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[2]/div/div/div/div/div/input"
                    ).send_keys(zipCode)
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div"
                    ).click()
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[3]/div/div/div/div/input"
                    ).send_keys(ville)
            except:
                try:
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[2]/div/div/div[1]/div/div/input"
                    ).send_keys(zipCode)
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div"
                    ).click()

                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[3]/div/div/div/div/input"
                    ).send_keys(ville)
                except Exception as e:
                    logging.error(e)
            # ADRESSE

            try:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[4]/div/div/div/div/div/input"
                ).send_keys(adresseDePublication)
            except:
                try:
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[2]/div/div/section/main/div[3]/div[5]/div/div/div/div/div/input"
                    ).send_keys(adresseDePublication)
                except:
                    pass
            # input("dfd")
            try:
                self.driver.find_element_by_xpath("/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[2]/div[3]/div/div/section/section/main/div[1]/div/div/div/div/div/input").click()
                sleep(1)
                self.driver.find_element_by_xpath("/html/body/div[3]/div[1]/div[1]/ul").click()
            except Exception as e:
                logging.error(e)
                pass

            # input("dded")
            logging.info("PAGE 1 DONE")
            uploadFile(
                project_id="38114472",
                file_path=self.fileName,
                fileName=self.fileNameName,
            )
            # PAGE 1
            # input("PAGE 1")
            # PAGE 1

            # On passe a la page 2
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/header/div/div/div[1]/div/div/div/div[3]"
            ).click()

            logging.info("PAGE 2 START")
            # ETAGE
            etageNum = str("".join(c for c in etage if c in digits))
            if etageNum == "" and etage in ["RDJ", "RDC"]:
                etageNum = "0"
            try:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[2]/div/div/div/div/input"
                ).send_keys(etageNum)
            except:
                pass

            # NOMBRE DE PIECES
            if "studio" in typologie or "":
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[5]/div/div/div/div/input"
                ).send_keys("1")
            else:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[5]/div/div/div/div/input"
                ).send_keys("".join(c for c in typologie if c in digits))

            # NOMBRE DE CHAMBRES
            if "studio" in typologie or chambreF == "":
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[6]/div/div/div/div/input"
                ).send_keys("1")
            else:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[6]/div/div/div/div/input"
                ).send_keys("".join(c for c in chambreF if c in digits))

            # SDB / SEAU / WC
            nbrSeau = 0
            nbrSDB = 0
            nbrWC = 0
            if nombreSdb == "une salle de bain avec wc":
                nbrSDB = 1
                nbrWC = 1
            elif nombreSdb == "une salle de bain et un wc séparé":
                nbrSDB = 1
                nbrWC = 1
            elif nombreSdb == "une salle d'eau avec wc":
                nbrSeau = 1
                nbrWC = 1
            elif nombreSdb == "une salle d'eau et un wc séparé":
                nbrSeau = 1
                nbrWC = 1
            elif nombreSdb == "2 salles de bain":
                nbrSDB = 2
            elif nombreSdb == "2 salles de bain et 2 wc":
                nbrSDB = 2
                nbrWC = 2
            elif nombreSdb == "une salle de bain et une salle d'eau avec wc ":
                nbrSDB = 1
                nbrSeau = 1
                nbrWC = 1
            elif nombreSdb == "une salle de bain, une salle d'eau et un wc séparé":
                nbrSDB = 1
                nbrSeau = 1
                nbrWC = 1
            elif nombreSdb == "une salle de bain":
                nbrSDB = 1
            elif nombreSdb == "une salle d'eau":
                nbrSeau = 1

            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[7]/div/div/div/div/input"
            ).send_keys(str(nbrSeau))
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[8]/div/div/div/div/input"
            ).send_keys(str(nbrSDB))
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[9]/div/div/div/div/input"
            ).send_keys(str(nbrWC))

            # WC SEPARE
            if "séparé" in nombreSdb:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[11]/label/span[1]/span"
                ).click()

            # PRESTATIONS INTERIEURES
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[12]/label/span[1]/span"
            ).click()
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[29]/label/span[1]/span"
            ).click()
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[1]/div[25]/label/span[1]/span"
            ).click()

            # SURFACE DU SEJOUR
            try:
                self.driver.find_element_by_xpath(
                    "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[1]/div/div/section/section/main/div[4]/div[1]/div/div/div/div/input"
                ).send_keys(surfacePieceAVivre)
            except:
                pass
            if "terrasse" in exterieurPrincipal and idLine in ["frydestéphaniedefaultb-4122-ae5b-10fc8b53ee07", "forestierflorencedefaultc-4d77-a413-766081a5cc33"]:
                try:
                    self.driver.find_element_by_xpath("/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[4]/div/div/div/div[2]/div/div[2]/label/span[1]/span").click()
                    sleep(0.3)
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[4]/div/div[4]/div/div/div/div[2]/div/div[19]/div/div/div/div/input"
                    ).send_keys(surfaceExterieurPrincipal)
                except:
                    pass
            logging.info("PAGE 2 DONE")
            uploadFile(
                project_id="38114472",
                file_path=self.fileName,
                fileName=self.fileNameName,
            )
            # PAGE 2 FIN
            # input("PAGE 2")
            # PAGE 2 FIN

            # On passe a la page 3
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/header/div/div/div[1]/div/div/div/div[4]"
            ).click()

            logging.info("PAGE 3 START")
            # PHOTOS
            for imgNum in range(1, 20):
                try:
                    os.remove(path.join(path.dirname(__file__), f"{str(imgNum)}.jpg"))
                except:
                    break
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[5]/div[1]/div/div/div[2]/button"
            ).click()
            self.driver.execute_script(
                'document.getElementsByClassName("el-upload__input")[0].removeAttribute("multiple");'
            )

            numPhotos = 1
            # EXTERIEUR
            # if "default" in idLine or idLine in ["169cb4ea-d9f9-40ef-8d45-14ef9e65e014"]:
            pathfirebase = "https://raw.githubusercontent.com/danhab05/facilitimopack/master/default/"
            pathF = ""
            if "loggia" in exterieurPrincipal:
                try:
                    if float(surfaceExterieurPrincipal.replace(",", ".")) >= 10.0:
                        pathF = pathfirebase + "Exterieur/Grande loggia"
                    else:
                        pathF = pathfirebase + "Exterieur/Petite loggia"
                except:
                    pathF = pathfirebase + "Exterieur/Petite loggia"
            elif "jardin" in exterieurPrincipal:
                pathF = pathfirebase + "Exterieur/Jardin"
            elif "petit" in exterieurPrincipal or "balcon" in exterieurPrincipal:
                pathF = pathfirebase + "Exterieur/Petit Exterieur"
            elif (
                "grand" in exterieurPrincipal
                or "terrasse" in exterieurPrincipal
                or exterieurPrincipal == "une terrasse"
                or "terrasses" in exterieurPrincipal
                or "2 terrasses" in exterieurPrincipal
            ):
                pathF = pathfirebase + "Exterieur/Grand Exterieur"
            randomNumber = random.randint(1, 4)
            if pathF != "":
                try:
                    image_url = pathF + "/" + str(randomNumber) + ".jpg"
                    image_url = image_url.replace(" ", "%20")  # the image on the web

                    logging.info(image_url)
                    save_name = path.join(
                        path.dirname(__file__), f"{str(numPhotos)}.jpg"
                    )
                    urllib.request.urlretrieve(image_url, save_name)

                    logging.info("ok")
                except Exception as e:
                    logging.error(e)
                    logging.info(image_url)
                    pass
                numPhotos += 1

            # INTERIEUR
            if "Local" in type:
                if residence in ["Vente"]:
                    pathF = "https://raw.githubusercontent.com/danhab05/facilitimopack/master/frydestephaniedefaultb-4122-ae5b-10fc8b53ee07/local/vente"
                else:
                    pathF = "https://raw.githubusercontent.com/danhab05/facilitimopack/master/frydestephaniedefaultb-4122-ae5b-10fc8b53ee07/local/location"

            elif "maison" in type or "villa" in type:
                if "3" in typologie:
                    pathF = pathfirebase + "Pack Maison 3 Pieces"
                elif "4" in typologie:
                    pathF = pathfirebase + "Pack Maison 4 Pieces"
                elif "5" in typologie:
                    pathF = pathfirebase + "Pack Maison 5 Pieces"
            elif "studio" in typologie:
                pathF = pathfirebase + "Pack studio"
            elif "duplex" in type:
                if "3" in typologie:
                    pathF = pathfirebase + "Pack Duplex 3 Pieces"
                elif "4" in typologie:
                    pathF = pathfirebase + "Pack Duplex 4 Pieces"
                elif "5" in typologie:
                    pathF = pathfirebase + "Pack Duplex 5 Pieces"
                else:
                    pathF = pathfirebase + "Pack Duplex 5 Pieces"
            else:
                if "2" in typologie:
                    pathF = pathfirebase + "Pack 2 Pieces"
                elif "3" in typologie:
                    pathF = pathfirebase + "Pack 3 Pieces"
                elif "4" in typologie:
                    pathF = pathfirebase + "Pack 4 Pieces"
                else:
                    pathF = pathfirebase + "Pack 5 Pieces"
            randomNumber = random.randint(1, 3)
            for imgNum in range(1, 25):
                try:
                    image_url = (
                        pathF + "/" + str(randomNumber) + "/" + str(imgNum) + ".jpg"
                    )
                    image_url = image_url.replace(" ", "%20")  # the image on the web

                    logging.info(image_url)
                    save_name = path.join(
                        path.dirname(__file__), f"{str(numPhotos)}.jpg"
                    )
                    urllib.request.urlretrieve(image_url, save_name)
                    logging.info("ok")
                    numPhotos += 1
                except Exception as e:
                    if "404" in str(e):
                        logging.error(e)
                        logging.info(image_url)
                        break
                    else:
                        logging.error(e)
                        logging.info(image_url)
                    pass

            # if "wc séparé" in nombreSdb:
            #     pathF = pathfirebase + "WC Separe"
            #     randomNumber = random.randint(1, 20)
            for i in range(1, numPhotos + 1):
                try:
                    self.driver.find_element_by_xpath(
                        "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[5]/div[1]/div/div/div[2]/div[1]/div/div[2]/div[1]/div/div/div[1]/input"
                    ).send_keys(path.join(path.dirname(__file__), f"{str(i)}.jpg"))
                    # sleep(0.3)
                except:
                    pass
            self.driver.find_element_by_xpath(
                "/html/body/section/section/div/main/div/form/section/main/div/div[2]/div[5]/div[1]/div/div/div[2]/div[1]/div/div[2]/div[2]/button[2]"
            ).click()

            sleep(4)
            try:
                self.driver.find_element_by_xpath("/html/body/section/section/div/main/span/section/main/div/div/div[1]/div[11]/div/div[1]/button/i").click()
            except:
                pass

            # Valider
            for _ in range(3):
                try:
                    self.driver.execute_script(
                        'document.getElementsByClassName("el-dropdown-menu el-popper")[2].getElementsByTagName("li")[0].click()'
                    )
                    sleep(5)

                    self.driver.execute_script(
                        'document.getElementsByClassName("CardContainer el-row")[0].getElementsByTagName("div")[0].getElementsByClassName("Card-footer")[0].getElementsByClassName("el-button el-button--default el-button--mini is-plain")[0].click()'
                    )
                    sleep(1)
                    idAnnouncing = self.driver.current_url
                    break
                except:
                    sleep(2)
                    idAnnouncing = "Erreur lien"
                    pass

            # TRANSFERT
            logging.info("Annonce publiee " + refAnnonce)
            uploadFile(
                project_id="38114472",
                file_path=self.fileName,
                fileName=self.fileNameName,
            )

    def getStatus(self):
        return self.driver.get_log("driver")


def func1():
    global driverF1
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/facilitimo")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    if str(uuid.getnode()) == "180725258811321":
        i3 = True
        try:  # remove chromedriver_mac_arm64.zip folder
            shutil.rmtree(os.getcwd() + "/chromedriver_mac64.zip")
        except:
            pass
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/facilitimo")
        chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
        driverFile = urllib.request.urlretrieve(
            "https://chromedriver.storage.googleapis.com/103.0.5060.134/chromedriver_mac64.zip",
            os.getcwd() + "/chromedriver_mac64.zip",
        )

        shutil.unpack_archive(os.getcwd() + "/chromedriver_mac64.zip", os.getcwd())
        os.chmod(os.getcwd() + "/chromedriver", 754)
        os.rename(os.getcwd() + "/chromedriver", "chromedriver1")
        driverF1 = webdriver.Chrome(
            os.getcwd() + "/chromedriver1", options=chrome_options
        )
        driverF1.get("https://facilitimo.web.app/")

    else:
        try:
            try:
                driverF1 = webdriver.Chrome(
                    ChromeDriverManager(chrome_type=ChromeType.BRAVE).install(),
                    options=chrome_options,
                )
            except Exception:
                driverF1 = webdriver.Chrome(
                    ChromeDriverManager().install(), options=chrome_options
                )
        except Exception:
            for ver in VERLIST:
                try:
                    if platform.system() == "Darwin":
                        if "arm" in platform.processor():
                            try:
                                # remove chromedriver_mac_arm64.zip folder
                                shutil.rmtree(os.getcwd() + "/chromedriver-mac-arm64")
                            except:
                                pass

                            driverFile = urllib.request.urlretrieve(
                                f"https://storage.googleapis.com/chrome-for-testing-public/{ver}/mac-arm64/chromedriver-mac-arm64.zip",
                                os.getcwd() + "/chromedriver_mac_arm64.zip",
                            )
                            shutil.unpack_archive(
                                os.getcwd() + "/chromedriver_mac_arm64.zip", os.getcwd()
                            )
                            os.chmod(
                                os.getcwd() + "/chromedriver-mac-arm64/chromedriver",
                                754,
                            )
                            os.rename(
                                os.getcwd() + "/chromedriver-mac-arm64/chromedriver",
                                "chromedriver1",
                            )
                        else:
                            try:
                                # remove chromedriver_mac_arm64.zip folder
                                shutil.rmtree(os.getcwd() + "/chromedriver_mac64.zip")
                            except:
                                pass
                            driverFile = urllib.request.urlretrieve(
                                "https://storage.googleapis.com/chrome-for-testing-public/"
                                + ver
                                + "/mac-x64/chromedriver-mac-x64.zip",
                                os.getcwd() + "/chromedriver_mac64.zip",
                            )
                            shutil.unpack_archive(
                                os.getcwd() + "/chromedriver_mac64.zip", os.getcwd()
                            )
                            os.chmod(
                                os.getcwd() + "/chromedriver-mac-x64/chromedriver", 754
                            )
                            os.rename(
                                os.getcwd() + "/chromedriver-mac-X64/chromedriver",
                                "chromedriver1",
                            )

                        driverF1 = webdriver.Chrome(
                            os.getcwd() + "/chromedriver1", options=chrome_options
                        )
                    else:
                        driverFile = urllib.request.urlretrieve(
                            f"https://storage.googleapis.com/chrome-for-testing-public/{ver}/win32/chromedriver-win32.zip",
                            os.getcwd() + "/chromedriver_win32.zip",
                        )
                        shutil.unpack_archive(
                            os.getcwd() + "/chromedriver_win32.zip", os.getcwd()
                        )
                        os.chmod(
                            os.getcwd() + "/chromedriver-win32/chromedriver.exe", 754
                        )
                        os.rename(
                            os.getcwd() + "/chromedriver-win32/chromedriver.exe",
                            "chromedriver1.exe",
                        )
                        driverF1 = webdriver.Chrome(
                            os.getcwd() + "/chromedriver1.exe", options=chrome_options
                        )
                    break
                except Exception as e:
                    logging.error(e)
                    try:
                        if platform.system() == "Darwin":

                            try:
                                os.remove(os.getcwd() + "/chromedriver1")
                            except:
                                pass
                        else:
                            os.remove(os.getcwd() + "/chromedriver1.exe")
                    except:
                        pass

    driverF1.get("https://facilitimo.web.app/")

    while True:
        state = driverF1.get_log("driver")
        if "disconnected" in str(state):
            driverF1.quit()
            break
        else:
            pickle.dump(driverF1.get_cookies(), open("facilitimo.pkl", "wb"))


def func2():
    app = Flask(__name__)
    CORS(app)

    scope = [
        "https://spreadsheets.google.com/feeds",
        "https://www.googleapis.com/auth/drive",
    ]

    def start_auto(idLine, txt):
        global quota
        client = None
        pathfirebase = (
            "https://raw.githubusercontent.com/danhab05/facilitimopack/master/"
            + idLine
            + "/"
        )
        while True:
            try:
                creds = ServiceAccountCredentials.from_json_keyfile_dict(
                    app_creds_dictionary, scope
                )
                client = gspread.authorize(creds)
                sheet = client.open("users")
                break
            except Exception as e:
                sleep(10)
        sheet_instance = sheet.get_worksheet(0)
        data_list = sheet_instance.get_all_records(numericise_ignore=["all"])
        data = None
        for el in data_list:
            if idLine == el["Id"]:
                data = el
                break
        userName = data["Nom d'utilisateur apimo"]
        password = data["Mot de passe apimo"]
        excelName = data["Nom Excel"]
        excelLink = data["Lien Excel"]
        quota = str(data["Quota"])
        fileNameName = data["Nom"] + "_" + data["Prénom"] + ".log"
        fileName = path.join(path.dirname(__file__), fileNameName + ".log")
        try:
            os.remove(fileName)
        except:
            pass
        logging.basicConfig(
            filename=fileName,
            filemode="w",
            level=logging.INFO,
            format="%(asctime)s - %(levelname)s - %(message)s",
        )
        # uploadFile(project_id="38114472", file_path=fileName, fileName=fileNameName)
        if platform.system() == "Windows":
            os.system("attrib +h " + fileName)
        try:
            scrap = AppImoAuto(
                userName=userName,
                excelLink=excelLink,
                passW=password,
                fileName=fileName,
                fileNameName=fileNameName,
                txt=txt,
            )
            # scrap.init()
            while True:
                r = scrap.login()
                if r == "Connected":
                    break
                sleep(1)
            scrap.openEx()
            # for data in records_data:
            L = []
            while True:
                logging.info("Here 1")
                L.clear()
                # sleep(30)
                while True:
                    try:

                        sheet = client.open(excelName)
                        sheet_instance = sheet.get_worksheet(0)
                        data_list = sheet_instance.get_all_records(
                            numericise_ignore=["all"]
                        )

                        sheet2 = client.open("users")
                        sheet_instance2 = sheet2.get_worksheet(0)
                        data_list_excel = sheet_instance2.get_all_records(
                            numericise_ignore=["all"]
                        )

                        sheet3 = client.open("usage")
                        sheet_instance3 = sheet3.get_worksheet(0)
                        break
                    except:
                        sleep(2)
                        creds = ServiceAccountCredentials.from_json_keyfile_dict(
                            app_creds_dictionary, scope
                        )
                        client = gspread.authorize(creds)
                        pass

                c = False
                for dataPivot in data_list:
                    if dataPivot["Terminé"] == "Ok" and dataPivot["Statuts"] not in [
                        "En ligne",
                        "Erreur",
                    ]:
                        logging.info("here 2")
                        c = True
                        break
                if c:
                    c = False
                    for i in data_list:
                        if i["Terminé"] == "Ok" and i["Statuts"] not in [
                            "En ligne",
                            "Erreur",
                        ]:
                            L.append(i)
                    logging.info("Annonce en attente: " + str(len(L)))
                    for data in L:
                        while True:
                            try:
                                dataUsers = None
                                for el in data_list_excel:
                                    if idLine == el["Id"]:
                                        dataUsers = el
                                        break
                                if data["Ville"] == "" or data["Ville"] is None:
                                    sheet_instance.update_cell(
                                        data_list.index(data) + 2,
                                        sheet_instance.col_count - 2,
                                        "En ligne",
                                    )
                                    sheet_instance.update_cell(
                                        data_list.index(data) + 2,
                                        sheet_instance.col_count - 1,
                                        "Annonce vide",
                                    )
                                elif data["Metre"] == "" or data["Metre"] is None:
                                    sheet_instance.update_cell(
                                        data_list.index(data) + 2,
                                        sheet_instance.col_count - 2,
                                        "En ligne",
                                    )
                                    sheet_instance.update_cell(
                                        data_list.index(data) + 2,
                                        sheet_instance.col_count - 1,
                                        "Pas de surface",
                                    )
                                else:
                                    sheet_instance.update_cell(
                                        data_list.index(data) + 2,
                                        sheet_instance.col_count - 2,
                                        "En cours de traitement",
                                    )
                                    now = datetime.datetime.now()
                                    timeDate = now.strftime("%Y-%m-%d %H:%M:%S")

                                    logging.info(
                                        "Annonce "
                                        + str(data_list.index(data) + 2)
                                        + " en cours de traitement"
                                    )
                                    try:

                                        try:
                                            surfacePieceAVivre = data["Surface Pièce à vivre"]
                                        except Exception:
                                            surfacePieceAVivre = ""
                                        try:
                                            surfaceChambrePrincpal = data[
                                                " Surfaces Chambre Principal"
                                            ]
                                        except Exception:
                                            surfaceChambrePrincpal = ""
                                        try:
                                            surfaceExterieurPrincipal = data[
                                                " Surface éxterieur 1"
                                            ]
                                        except Exception:
                                            surfaceExterieurPrincipal = ""
                                        try:
                                            adresseDePublication = data[
                                                "Adresse de publication"
                                            ]
                                        except Exception:
                                            adresseDePublication = ""
                                        try:
                                            situationGeographique = data[
                                                "Situation géographique"
                                            ]
                                        except Exception:
                                            situationGeographique = ""
                                        try:
                                            date = data["Date de livraison"]
                                        except Exception:
                                            date = ""
                                        try:
                                            chambre = data[" Chambre"]
                                        except Exception:
                                            chambre = ""
                                        try:
                                            nombreSdb = data[" Nombe SDB/WC"]
                                        except Exception:
                                            nombreSdb = ""

                                        try:
                                            residence = data["Résidence"]
                                        except Exception:
                                            residence = ""

                                        try:
                                            complementAd = data["Complement d'adresse"]
                                        except Exception:
                                            complementAd = ""
                                        scrap.addAnnouncing(
                                            complementAd=complementAd,
                                            pathfirebase=pathfirebase,
                                            refAnnonce=str(data["Ref Annonce"])
                                            .replace(" ", "")
                                            .replace("/", "")
                                            .replace("-", ""),
                                            ville=data["Ville"],
                                            adresseDePublication=adresseDePublication,
                                            zipCode=data["Code Postal"].replace(" ", ""),
                                            residence=residence,
                                            type=data["Type"],
                                            typologie=data["Typologie"],
                                            etage=data["Etage"],
                                            metre=str(data["Metre"]).replace(",", "."),
                                            prix=data["Prix"],
                                            parkingCave=data["Parking/Cave"],
                                            surfacePieceAVivre=surfacePieceAVivre,
                                            exterieurPrincipal=data[" Exterieur principale"],
                                            surfaceExterieurPrincipal=surfaceExterieurPrincipal,
                                            chambre=chambre,
                                            surfaceChambrePrincpal=surfaceChambrePrincpal,
                                            nombreSdb=nombreSdb,
                                            situationGeographique=situationGeographique,
                                            date=date,
                                            titre=data["Titre annonce"],
                                            nameAgent="Lesslie",
                                            tva=data["Tva reduite"],
                                            phoneNumber="06 22 71 09 22",
                                            idLine=idLine,
                                        )

                                        while True:
                                            try:

                                                if idAnnouncing == "Limite d'annonce atteinte":
                                                    sheet_instance.update_cell(
                                                        data_list.index(data) + 2,
                                                        sheet_instance.col_count - 2,
                                                        "Erreur",
                                                    )

                                                else:
                                                    sheet_instance.update_cell(
                                                        data_list.index(data) + 2,
                                                        sheet_instance.col_count - 2,
                                                        "En ligne",
                                                    )
                                                sheet_instance.update_cell(
                                                    data_list.index(data) + 2,
                                                    sheet_instance.col_count - 1,
                                                    idAnnouncing,
                                                )
                                                sheet_instance.update_cell(
                                                    data_list.index(data) + 2,
                                                    sheet_instance.col_count,
                                                    timeDate,
                                                )
                                                break
                                            except:
                                                creds = ServiceAccountCredentials.from_json_keyfile_dict(
                                                    app_creds_dictionary, scope
                                                )
                                                client = gspread.authorize(creds)
                                                sheet = client.open(excelName)
                                                sheet_instance = sheet.get_worksheet(0)
                                                data_list = sheet_instance.get_all_records(
                                                    numericise_ignore=["all"]
                                                )

                                                sheet2 = client.open("users")
                                                sheet_instance2 = sheet2.get_worksheet(0)
                                                data_list_excel = (
                                                    sheet_instance2.get_all_records(
                                                        numericise_ignore=["all"]
                                                    )
                                                )

                                                sheet3 = client.open("usage")
                                                sheet_instance3 = sheet3.get_worksheet(0)

                                        try:
                                            sheet_instance3.insert_row(
                                                [
                                                    dataUsers["Nom"],
                                                    dataUsers["Prénom"],
                                                    timeDate,
                                                ],
                                                index=3,
                                            )
                                        except Exception as e:
                                            logging.error(e)

                                    except Exception as e:
                                        if "Unable to evaluate script: disconnected" in str(
                                            scrap.getStatus()
                                        ):
                                            logging.info(
                                                "Browser window closed by user, stoping..."
                                            )
                                            # uploadFile(
                                            #     project_id="38114472",
                                            #     file_path=fileName,
                                            #     fileName=fileNameName,
                                            # )
                                            # scrap.init()
                                            # scrap.login()
                                            # sleep(1)
                                            # logging.info("Restarded successfully")
                                            # uploadFile(project_id="38114472",
                                            #            file_path=fileName,
                                            #            fileName=fileNameName)
                                        else:
                                            logging.exception(e)
                                            uploadFile(
                                                project_id="38114472",
                                                file_path=fileName,
                                                fileName=fileNameName,
                                            )
                                break
                            except Exception as e:
                                logging.error(e)
                                sleep(2)
                                try:
                                    creds = ServiceAccountCredentials.from_json_keyfile_dict(
                                        app_creds_dictionary, scope
                                    )
                                    client = gspread.authorize(creds)
                                    sheet = client.open(excelName)
                                    sheet_instance = sheet.get_worksheet(0)
                                    data_list = sheet_instance.get_all_records(
                                        numericise_ignore=["all"]
                                    )

                                    sheet2 = client.open("users")
                                    sheet_instance2 = sheet2.get_worksheet(0)
                                    data_list_excel = sheet_instance2.get_all_records(
                                        numericise_ignore=["all"]
                                    )

                                    sheet3 = client.open("usage")
                                    sheet_instance3 = sheet3.get_worksheet(0)
                                    pass
                                except:
                                    pass
                    scrap.backHome()
                if "Unable to evaluate script: disconnected" in str(scrap.getStatus()):
                    logging.info("Browser window closed by user, stoping...")
                    uploadFile(
                        project_id="38114472", file_path=fileName, fileName=fileNameName
                    )
                    raise Exception("Browser window closed by user")

                sleep(2)
        except Exception as e:
            logging.exception(e)
            uploadFile(project_id="38114472", file_path=fileName, fileName=fileNameName)

    @app.route("/")
    def home():
        r = "ok"
        return r

    def shutdown_server():
        func = request.environ.get("werkzeug.server.shutdown")
        if func is None:
            raise RuntimeError("Not running with the Werkzeug Server")
        func()

    @app.route("/shutdown", methods=["GET"])
    def shutdown():
        shutdown_server()

    @app.route("/version")
    def version():
        return VERSIONPROG

    @app.route("/login")
    def login():
        email = request.args.get("email")
        password = request.args.get("password")
        creds = ServiceAccountCredentials.from_json_keyfile_dict(
            app_creds_dictionary, scope
        )
        while True:
            try:
                client = gspread.authorize(creds)
                sheet = client.open("users")
                sheet_instance = sheet.get_worksheet(0)
                break
            except Exception as e:
                sleep(3)
                print(e)
                pass
        data_list = sheet_instance.get_all_records(numericise_ignore=["all"])
        connected = False
        data = None
        for el in data_list:
            if email == el["Email"] and password == el["Mot de passe"]:
                connected = True
                data = el
                break
            else:
                connected = False
        if connected:
            return data
        else:
            return "Not connected"

    @app.route("/start")
    def start():
        idLine = request.args.get("idLine")
        txt = request.args.get("txt")
        start_auto(idLine, txt)
        return "Ok"

    @app.route("/open")
    def openFolder():
        folder = request.args.get("folder")
        print(folder)
        if "mac" in platform.platform():
            subprocess.call(f"open {folder}", shell=True)
        else:
            subprocess.call(f"explorer.exe {folder}", shell=True)
        return "not yet"

    @app.route("/add")
    def add():
        try:
            idLine = request.args.get("idLine")
            name = request.args.get("name")
            firstName = request.args.get("firstName")
            email = request.args.get("email")
            password = request.args.get("password")
            apimoUsername = request.args.get("apimoUsername")
            apimoPassword = request.args.get("apimoPassword")
            admin = request.args.get("admin")

            while True:
                try:
                    creds = ServiceAccountCredentials.from_json_keyfile_dict(
                        app_creds_dictionary, scope
                    )
                    client = gspread.authorize(creds)
                    sheet = client.open("users")
                    sheet_instance = sheet.get_worksheet(0)
                    data_list = sheet_instance.get_all_records(
                        numericise_ignore=["all"]
                    )
                    data = None
                    # uid = str(uuid.uuid4())
                    # uid = uid[7:]
                    # uid = name + firstName + uid
                    # for el in data_list:
                    #     if idLine == el['Id']:
                    #         data = el
                    #         break
                    # if data["Compte admin"] == "admin":
                    excelName = "apimo" + "_" + name + "_" + firstName
                    excel = client.copy(
                        "1oBTuzcjJ4TWiJNXgNKzeRjlRrW4Eit2Lw1htD0l2q98",
                        title=excelName,
                        folder_id="1yYAqno01ENCYBDsf3-Rih0eF-EmhTUOr",
                    )
                    # change owner of the copied file

                    # Share excel to anyone allowing them to edit
                    excel.share(None, perm_type="anyone", role="writer")
                    sheet_instance.append_row(
                        [
                            idLine,
                            name,
                            firstName,
                            email,
                            password,
                            apimoUsername,
                            apimoPassword,
                            excelName,
                            admin,
                            "https://docs.google.com/spreadsheets/d/%s" % excel.id,
                            300,
                        ]
                    )
                    break
                except Exception as e:
                    pass

            return "ok"
        except Exception as e:
            return str(e)

    app.run(debug=False, port=7654)


def shutdown():
    if platform.system() == "Windows":
        os.system("echo stop")
    else:
        os.system("kill -9 $(lsof -ti:7654)")


def func3():
    while True:
        sleep(30)
        pass


def func4():
    r = req.get(
        """http://127.0.0.1:7654/start?idLine=169cb4ea-d9f9-40ef-8d45-14ef9e65e014&&txt=BLG immobilier vous présente:
        Beau {Typologie} de {Metre}m² dans une future belle résidence de standing localisée en centre-ville, {Situation_géographique}. Disponible {date}

Comprend:
        - une entrée
        - un séjour/cuisine US
{Chambre} 
        - {Nombe_SDB/WC}

{Exterieur_principale}{Surface_éxterieur}

{Parking/Cave}

{Tva_reduite}
Frais de notaires réduits et faibles charges.
Logement BBC, Normes RT2012 et HQE. Logement NF, personnalisable sur demande.
Accessible PMR.
Photos non contractuelles, suggestions d'aménagements.
Plans, rendez-vous d’information et réservation sur demande. Joignable 7J/7.

Leslie 06 22 71 09 22
&&pathfirebase=/Users/leslie/Desktop/imagefacilitimo/"""
    )
    while True:
        pass


def func5():

    # check if platform is linux
    if "Linux" in platform.platform():

        # make a flask app on server 8096
        app = Flask(__name__)

        @app.route("/")
        def quit():
            os._exit(0)

        app.run(debug=False, port=8096, host="0.0.0.0")
    else:
        pass


if __name__ == "__main__":
    if "mac" in platform.platform():
        os.system("killall Google\ Chrome >/dev/null 2&>1")
    else:
        try:
            os.system("taskkill /F /IM chromedriver.exe")
            os.system("taskkill /F /IM chromedriver1.exe")
            for file in os.listdir(os.getcwd()):
                if "chromedriver" in file:
                    os.remove(file)
        except Exception:
            pass
        pass
        # os.system("taskkill /F /IM chrome.exe")
    try:
        os.remove(os.getcwd() + "/chromedriver")
    except Exception:
        pass
    try:
        os.remove(os.getcwd() + "/chromedriver.exe")
    except Exception:
        pass
    try:
        os.remove(os.getcwd() + "/chromedriver1")
    except Exception:
        pass
    try:
        os.remove(os.getcwd() + "/chromedriver1.exe")
    except Exception:
        pass
    args = ""
    try:
        args = sys.argv[1]
    except:
        pass
    if args == "direct":
        thread = Thread(target=func2)
        thread2 = Thread(target=func4)
        thread3 = Thread(target=func5)
        thread.start()
        thread3.start()
        sleep(2)
        thread2.start()
        while thread.is_alive() or thread2.is_alive():
            sleep(1)
            if not thread.is_alive():
                if "mac" in platform.platform():
                    os.system("killall Google\ Chrome >/dev/null 2&>1")
                    os.system("pkill -a Terminal")
                else:
                    pass
                    # os.system("taskkill /F /IM chrome.exe")
                os._exit(0)
            if not thread2.is_alive():
                if "mac" in platform.platform():
                    os.system("killall Google\ Chrome >/dev/null 2&>1")
                    os.system("pkill -a Terminal")
                else:
                    pass
                    # os.system("taskkill /F /IM chrome.exe")
                os._exit(0)
    else:
        # create thread between func1 and func2
        thread = Thread(target=func1)
        thread2 = Thread(target=func2)
        thread3 = Thread(target=func3)
        thread2.start()
        thread.start()
        thread3.start()
        # detect if one thread is close then stop the script
        while thread.is_alive() or thread2.is_alive():
            sleep(1)
            if not thread.is_alive():
                if "mac" in platform.platform():
                    os.system("killall Google\ Chrome >/dev/null 2&>1")
                    os.system("pkill -a Terminal")
                else:
                    pass
                    # os.system("taskkill /F /IM chrome.exe")
                os._exit(0)
            if not thread2.is_alive():
                if "mac" in platform.platform():
                    os.system("killall Google\ Chrome >/dev/null 2&>1")
                    os.system("pkill -a Terminal")
                else:
                    pass
                    # os.system("taskkill /F /IM chrome.exe")
                os._exit(0)
