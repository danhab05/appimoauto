#!/bin/bash
# used to remove com.apple.quarantine attribute from a given file/pkg
#
# use this like: xattr_remove /path/to/file
cd $1
find . -print0 | xargs -0 xattr -d com.apple.quarantine

