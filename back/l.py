# make a function script who download a file from a url and execute it with arguments "direct" 
import os
import time
import requests
import schedule
import wget

def downloadAndExecute():
    url = "https://gitlab.com/danhab05/appimoauto/-/raw/main/back/facilitimoprog.py"
    filename = wget.download(url)
    os.system("python facilitimoprog.py direct")
    


# schedule every day except saturday at 18:00 to run downloadAndExecute()
schedule.every().day.monday.at("18:00").do(downloadAndExecute)
schedule.every().day.tuesday.at("18:00").do(downloadAndExecute)
schedule.every().day.wednesday.at("18:00").do(downloadAndExecute)
schedule.every().day.thursday.at("18:00").do(downloadAndExecute)
schedule.every().day.friday.at("18:00").do(downloadAndExecute)
schedule.every().day.sunday.at("17:09").do(downloadAndExecute)


while True:
    schedule.run_pending()
    time.sleep(1)

