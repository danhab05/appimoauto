// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import "package:http/http.dart" as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uiappimo/adduser.dart';
import 'package:uiappimo/error.dart';
import 'package:uiappimo/main.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, this.uid}) : super(key: key);
  final uid;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void insertText(String myText) {
    final text = txtData.text;
    final textSelection = txtData.selection;
    final newText = text.replaceRange(
      textSelection.start,
      textSelection.end,
      myText,
    );
    final myTextLength = myText.length;
    txtData.text = newText;
    txtData.selection = textSelection.copyWith(
      baseOffset: textSelection.start + myTextLength,
      extentOffset: textSelection.start + myTextLength,
    );
  }

  var txt = "";
  var txtData = TextEditingController();
  var prefs;
  var data;
  var admin = false;
  var id = "";
  var url = "";
  var url2 = "";
  var saveButton = true;
  var firestore = FirebaseFirestore.instance;
  getPrefs() async {
    prefs = await SharedPreferences.getInstance();
    id = prefs.getString('id');
    setState(() {
      url = "http://127.0.0.1:7654/name?idLine=$id";
      url2 = "http://127.0.0.1:7654/version";
    });
    firestore.collection("users").get().then((value) {
      for (var element in value.docs) {
        if (element.data()['id'] == id) {
          setState(() {
            txt = element.data()['txt'];
            txtData.text = txt;
            path = element.data()['path'];
          });
        }
      }
    });
    // addName();
    setState(() {});
  }

  addName() async {
    var err = "";
    try {
      await http.get(Uri.parse(url2)).then((res) {
        setState(() {
          version = res.body;
        });
      });
    } catch (e) {
      err = e.toString();
    }
    try {
      await http.get(Uri.parse(url)).then((res) {
        setState(() {
          data = json.decode(res.body);
        });
      });
    } catch (e) {
      err = e.toString();
    }
    if (err.contains("XMLHttpRequest ")) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const ErrorPage()));
    }

    setState(() {
      firstName = data["Prénom"];
      if (data["Compte admin"] == "admin") {
        admin = true;
      }
    });
  }

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isVisible = true;
  var firstName = '';
  var version = "";
  var path = "";
  @override
  void initState() {
    super.initState();
    getPrefs();
  }

  var listData = [
    'Ref_Annonce',
    'Ville',
    'Code_Postal',
    'Adresse_de_publication',
    'Résidence',
    'Type',
    'Typologie',
    'Etage',
    'Metre',
    'Prix',
    'Tva_reduite',
    'Parking/Cave',
    'Surface_Pièce_à_vivre',
    'Exterieur_principale',
    'Surface_éxterieur',
    'Chambre',
    'Surfaces_chambre_Principal',
    'Nombe_SDB/WC',
    'Situation_géographique',
    'Titre_annonce',
    'Date_de_livraison'
  ];
  ScrollController myController = ScrollController();
  var anchor = GlobalKey();
  var pxScroll = 250.0;
  var scrollEnd = 0.0;
  var bytes;
  var fileName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff252526),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            opacity: 0.4,
            image: NetworkImage(
                "https://cdn.pixabay.com/photo/2016/11/29/03/53/house-1867187_1280.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Stack(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppBar(
                centerTitle: true,
                actions: [
                  admin
                      ? ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const AddUser()),
                            );
                          },
                          child: const Icon(Icons.add),
                        )
                      : Container()
                ],
                title: FittedBox(
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ButtonStyle(
                        elevation: MaterialStateProperty.all<double>(0),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.transparent)),
                    onLongPress: () async {
                      var prefs = await SharedPreferences.getInstance();
                      await prefs.clear();
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  const LoginPage()));
                    },
                    child: Text(
                      "Facilitimo $version",
                      style: const TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                ),
                backgroundColor: Colors.transparent,
                elevation: 0,
              ),
              const Text(
                "Bonjour",
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              Center(
                  child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 15,
                                right: 8,
                                bottom: 8,
                              ),
                              child: IconButton(
                                  onPressed: () {
                                    print(pxScroll);
                                    if (myController.offset > 0) {
                                      print("here");
                                      setState(() {});
                                      myController.animateTo(pxScroll,
                                          duration: const Duration(
                                              milliseconds: 1000),
                                          curve: Curves.fastOutSlowIn);
                                      // myController.position.ensureVisible(
                                      //   anchor.currentContext.findRenderObject(),
                                      //   alignment: 0.5,
                                      //   duration: const Duration(seconds: 1),
                                      // );
                                      setState(() {
                                        scrollEnd = myController.offset;
                                        pxScroll = pxScroll - 250.0;
                                      });
                                    }
                                  },
                                  icon: const Icon(
                                    Icons.arrow_left,
                                    size: 35,
                                    color: Colors.white,
                                  )),
                            ),
                            Flexible(
                              child: SingleChildScrollView(
                                controller: myController,
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: [
                                    for (var el in listData)
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8, right: 8, bottom: 8),
                                        child: ElevatedButton(
                                          onPressed: () {
                                            insertText("{$el}");
                                          },
                                          child: Text(el),
                                        ),
                                      ),
                                    // const Text(
                                    //   ">",
                                    //   style: TextStyle(color: Colors.white),
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 15,
                                right: 8,
                                bottom: 8,
                              ),
                              child: IconButton(
                                  onPressed: () {
                                    print(pxScroll);
                                    if (myController.position.maxScrollExtent >
                                        myController.offset) {
                                      setState(() {});
                                      myController.animateTo(pxScroll,
                                          duration: const Duration(
                                              milliseconds: 1000),
                                          curve: Curves.fastOutSlowIn);
                                      // myController.position.ensureVisible(
                                      //   anchor.currentContext.findRenderObject(),
                                      //   alignment: 0.5,
                                      //   duration: const Duration(seconds: 1),
                                      // );
                                      setState(() {
                                        scrollEnd = myController.offset;
                                        pxScroll = pxScroll + 250.0;
                                      });
                                    } else if (myController.offset ==
                                        scrollEnd) {}
                                  },
                                  icon: const Icon(
                                    Icons.arrow_right,
                                    size: 35,
                                    color: Colors.white,
                                  )),
                            ),
                          ]),
                        ),
                        Flexible(
                          child: TextField(
                            // onTap: () {
                            //   setState(() {
                            //     saveButton = true;
                            //   });
                            // },
                            decoration: InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.blue, width: 0.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 0.0),
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            controller: txtData,
                            style: const TextStyle(
                                color: Colors.white, fontSize: 17),
                            keyboardType: TextInputType.multiline,
                            minLines:
                                20, //Normal textInputField will be displayed
                            maxLines:
                                22, // when user presses enter it will adapt to it
                          ),
                        ),
                        Visibility(
                          visible: saveButton,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onLongPress: (() {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const AddUser()),
                                );
                              }),
                              onPressed: (() {
                                firestore
                                    .collection("users")
                                    .get()
                                    .then((value) {
                                  for (var element in value.docs) {
                                    if (element.data()['id'] == id) {
                                      firestore
                                          .collection("users")
                                          .doc(element.id)
                                          .update({'txt': txtData.text});
                                    }
                                  }
                                });
                                // setState(() {
                                //   saveButton = false;
                                // });
                              }),
                              child: const Text("Enregistrer"),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      onPressed: () async {
                        firestore.collection("users").get().then((value) {
                          for (var element in value.docs) {
                            if (element.data()['id'] == id) {
                              firestore
                                  .collection("users")
                                  .doc(element.id)
                                  .update({'txt': txtData.text});
                            }
                          }
                        });
                        var err = "";
                        try {
                          await http
                              .get(Uri.parse("http://127.0.0.1:7654/"))
                              .then((value) {
                            print(value.statusCode);
                          });
                        } catch (e) {
                          err = e.toString();
                        }
                        if (err.contains("XMLHttpRequest ")) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ErrorPage()));
                        }
                        http
                            .get(Uri.parse(
                                "http://127.0.0.1:7654/start?idLine=$id&&txt=${txtData.text}&&pathfirebase=$path"))
                            .then((response) {});
                        // showdialog that disapear after 3 sec
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              Future.delayed(const Duration(seconds: 3), () {
                                Navigator.of(context).pop(true);
                              });
                              return AlertDialog(
                                backgroundColor: Colors.blueGrey[900],
                                title: const Text(
                                  "L'automatisation a bien été lancée, ",
                                  style: TextStyle(color: Colors.white),
                                ),
                                content: const Text(
                                    "Veuillez patienter quelques secondes...",
                                    style: TextStyle(color: Colors.white)),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text("OK"),
                                  ),
                                ],
                              );
                            });
                      },
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all<OutlinedBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                        minimumSize: MaterialStateProperty.all<Size>(
                          const Size(200, 80),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.blue),
                      ),
                      child: const Text("Lancer l'automatisation"),
                    ),
                  ),
                  const SizedBox(
                    width: 25,
                  ),
                  // Image.asset(
                  //   "assets/img.png",
                  //   height: MediaQuery.of(context).size.height / 2.40,
                  //   width: MediaQuery.of(context).size.width / 4.8,
                  // )
                ],
              )),
              const SizedBox(
                height: 90,
              ),
              Positioned(
                bottom: 0,
                left: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  color: Colors.transparent,
                  child: const Center(
                    child: Text(
                      "© 2024 - Dan Habib",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
