// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyC__E01feOa1Yyv0slSAQExrw9LyX7NRHs',
    appId: '1:19279972057:web:7956de3c720ffcdbd6a0c8',
    messagingSenderId: '19279972057',
    projectId: 'facilitimo',
    authDomain: 'facilitimo.firebaseapp.com',
    storageBucket: 'facilitimo.appspot.com',
    measurementId: 'G-7PSYNMDCYB',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyAfg0r_RP41fO945bAHH6AePe4X50KJglU',
    appId: '1:19279972057:android:b492f320249c08e9d6a0c8',
    messagingSenderId: '19279972057',
    projectId: 'facilitimo',
    storageBucket: 'facilitimo.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyA3laDEO3scR2Pt8vP8hzZT56U-dvzBZ48',
    appId: '1:19279972057:ios:498fe89bc53480a4d6a0c8',
    messagingSenderId: '19279972057',
    projectId: 'facilitimo',
    storageBucket: 'facilitimo.appspot.com',
    iosClientId: '19279972057-badouah7t06l48nh2a7dgfuts4j50ka6.apps.googleusercontent.com',
    iosBundleId: 'com.example.uiappimo',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyA3laDEO3scR2Pt8vP8hzZT56U-dvzBZ48',
    appId: '1:19279972057:ios:498fe89bc53480a4d6a0c8',
    messagingSenderId: '19279972057',
    projectId: 'facilitimo',
    storageBucket: 'facilitimo.appspot.com',
    iosClientId: '19279972057-badouah7t06l48nh2a7dgfuts4j50ka6.apps.googleusercontent.com',
    iosBundleId: 'com.example.uiappimo',
  );
}
