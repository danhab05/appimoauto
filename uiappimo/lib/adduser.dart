// ignore_for_file: use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uiappimo/Confirmation.dart';
import 'package:uuid/uuid.dart';

var uuid = const Uuid();

class AddUser extends StatefulWidget {
  const AddUser({super.key});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      name.clear();
      firstName.clear();
      email.clear();
      password.clear();
      apimoUsername.clear();
      apimoPassword.clear();
      admin = false;
    });
  }

  var loading = false;

  TextEditingController name = TextEditingController();
  TextEditingController firstName = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController apimoUsername = TextEditingController();
  TextEditingController phonenumber = TextEditingController();
  TextEditingController apimoPassword = TextEditingController();
  bool admin = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff252526),
      appBar: AppBar(
        title: const Text('Ajouter un utilisateur'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            TextField(
              controller: name,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text('Nom',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: firstName,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text('Prénom',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: phonenumber,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text('Téléphone',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: email,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text('identifiant programme',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              // obscureText: true,
              controller: password,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text('Mot de passe programme',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: apimoUsername,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text("Nom d'utilisateur apimo",
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: apimoPassword,
              // obscureText: true,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                label: Text('Mot de passe apimo',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(110, 34, 17, 17)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 255, 212, 224)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Color.fromARGB(255, 66, 125, 145)),
                ),
              ),
            ),
            const SizedBox(
              height: 35,
            ),
            CheckboxListTile(
              title: const Text('Compte administrateur ?',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              value: admin,
              onChanged: (bool? value) {
                setState(() {
                  admin = !admin;
                });
              },
            ),
            const SizedBox(
              height: 35,
            ),
            loading ? const CircularProgressIndicator() : const SizedBox(),
            Row(
              children: [
                const Spacer(),
                ElevatedButton(
                    onPressed: () async {
                      setState(() {
                        loading = true;
                      });
                      var adminTxt = "";
                      var id = uuid.v4();
                      // remove 7 first characters of id
                      id = id.substring(12);
                      id =
                          "${name.text.toLowerCase()}${firstName.text.toLowerCase()}default$id";
                      if (admin) {
                        adminTxt = "admin";
                      } else {
                        adminTxt = "";
                      }
                      FirebaseFirestore.instance
                          .collection("users")
                          .doc(id)
                          .set({
                        "id": id,
                        "txt": """
BLG immobilier vous présente:
Beau {Typologie} de {Metre}m² dans une future belle résidence de standing localisée en centre-ville, {Situation_géographique}. Disponible {date}

Comprend:
        - une entrée
        - un séjour/cuisine US
{Chambre} 
        - {Nombe_SDB/WC}

{Exterieur_principale}{Surface_éxterieur}

{Parking/Cave}

{Tva_reduite}
Frais de notaires réduits et faibles charges.
Logement BBC, Normes RT2012 et HQE. Logement NF, personnalisable sur demande.
Accessible PMR.
Photos non contractuelles, suggestions d'aménagements.
Plans, rendez-vous d’information et réservation sur demande. Joignable 7J/7.
${firstName.text}: ${phonenumber.text}""",
                      });
                      await http.get(Uri.parse(
                          "http://127.0.0.1:7654/add?idLine=$id&&name=${name.text}&&firstName=${firstName.text}&&email=${email.text}&&password=${password.text}&&apimoUsername=${apimoUsername.text}&&apimoPassword=${apimoPassword.text}&&admin=$adminTxt"));
                      setState(() {
                        loading = false;
                      });
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Confirmation(
                                  text:
                                      "L'utilisateur a bien été ajouté\nIdentifiant : ${email.text}\nMot de passe : ${password.text}",
                                )),
                      );
                    },
                    child: const Text('Ajouter')),
              ],
            )
          ],
        ),
      ),
    );
  }
}
