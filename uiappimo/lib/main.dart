// ignore_for_file: use_build_context_synchronously, invalid_use_of_visible_for_testing_member

import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uiappimo/error.dart';
import 'package:uiappimo/firebase_options.dart';
import 'package:uiappimo/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:url_strategy/url_strategy.dart';

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}

void main() async {
  setPathUrlStrategy();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  var err = "";
  try {
    await http.get(Uri.parse("http://127.0.0.1:7654/")).then((value) {
      print(value.statusCode);
    });
  } catch (e) {
    err = e.toString();
  }
  if (err.contains("XMLHttpRequest ")) {
    runApp(MaterialApp(
      scrollBehavior: MyCustomScrollBehavior(),
      debugShowCheckedModeBanner: false,
      home: const ErrorPage(),
    ));
  } else {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final status = prefs.getString('connected');
    runApp(MaterialApp(
      scrollBehavior: MyCustomScrollBehavior(),
      debugShowCheckedModeBanner: false,
      home: status == "connected" ? const HomePage() : const LoginPage(),
    ));
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
  }

  var firstName = "";
  bool showLoading = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isVisible = true;
  var err = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red.shade300,
      body: Container(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              child: Image.asset(
                'assets/images/profile.png',
                height: 200,
                width: 200,
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                constraints: const BoxConstraints.expand(),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(35),
                        topRight: Radius.circular(35))),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 50,
                      ),
                      const Text(
                        'Connexion',
                        style: TextStyle(
                            fontSize: 32, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextField(
                        controller: emailController,
                        onChanged: (value) {
                          setState(() {});
                        },
                        decoration: InputDecoration(
                            // icon: Icon(Icons.mail),
                            prefixIcon: const Icon(Icons.mail),
                            suffixIcon: emailController.text.isEmpty
                                ? const Text('')
                                : GestureDetector(
                                    onTap: () {
                                      emailController.clear();
                                    },
                                    child: const Icon(Icons.close)),
                            hintText: 'exemple@mail.com',
                            labelText: 'Email',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: const BorderSide(
                                    color: Colors.red, width: 1))),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextField(
                        obscureText: isVisible,
                        controller: passwordController,
                        decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.lock),
                            suffixIcon: GestureDetector(
                                onTap: () {
                                  isVisible = !isVisible;
                                  setState(() {});
                                },
                                child: Icon(isVisible
                                    ? Icons.visibility_off
                                    : Icons.visibility)),
                            hintText: 'Mot de passe',
                            labelText: 'Mot de passe',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: const BorderSide(
                                    color: Colors.red, width: 1))),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      showLoading
                          ? const CircularProgressIndicator()
                          : Text(
                              err,
                              style: const TextStyle(
                                  color: Colors.red, fontSize: 17),
                            ),
                      const SizedBox(
                        height: 16,
                      ),
                      ElevatedButton(
                          onPressed: () async {
                            setState(() {
                              err = "";
                              showLoading = true;
                            });
                            var status = "";
                            await http
                                .get(Uri.parse(
                                    "http://127.0.0.1:7654/login?email=${emailController.text}&&password=${passwordController.text}"))
                                .then((response) {
                              status = response.body;
                            });
                            if (status != "Not connected") {
                              var data = jsonDecode(status);
                              final prefs =
                                  await SharedPreferences.getInstance();
                              await prefs.setString('id', data["Id"]);
                              await prefs.setString('connected', "connected");

                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const HomePage(),
                                ),
                              );
                            } else {
                              setState(() {
                                err = "Email ou mot de passe incorrect";
                              });
                            }
                            setState(() {
                              showLoading = false;
                            });
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 10),
                            child: Text('Se connecter'),
                          ))
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
