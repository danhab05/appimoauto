// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uiappimo/main.dart';

class ErrorPage extends StatefulWidget {
  const ErrorPage({Key? key, this.uid}) : super(key: key);
  final uid;
  @override
  _ErrorPageState createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: ElevatedButton(
          onPressed: () {},
          style: ButtonStyle(
              elevation: MaterialStateProperty.all<double>(0),
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.transparent)),
          onLongPress: () async {
            var prefs = await SharedPreferences.getInstance();
            await prefs.clear();
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => const LoginPage()));
          },
          child: const Text("AppImoAuto"),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: const Color(0xff252526),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Center(
                child: Text(
              "Bonjour veuillez d'abbord lancer le programme present sur votre ordinateur",
              style: TextStyle(color: Colors.white, fontSize: 25),
            )),
          ],
        ),
      ),
    );
  }
}
